var assert = require('assert');

var leftpad = require('./library.js');

describe('leftpad', function() {

  it('should pad str when padLen > str.length', function() {
    assert.equal(leftpad("bcd", 4, "a"), "abcd");
  });

  it('should be as long as padLen when padLen > str.length', function() {
    assert.equal(leftpad("234", 4, "1").length, 4);
  });


  it('should return same str when padLen == str.length', function() {
    var str = "abcd";
    assert.equal(leftpad(str, 4, "a"), str);
  });

  it('should return str when padLen < str.length', function() {
    var str = "abcde";
    assert.equal(leftpad(str, 4, "a"), str);
  });
  
});