/**
 * Pad a string to at least a given length with a specified padding.
 * @param {string} str - string to pad
 * @param {number} len - minimum length of string
 * @param {string} padStr - string to use for padding
 */
function leftpad(str, padLen, padStr) {
    str = (str || "").toString();
    
    var len = Math.max(padLen - str.length + 1, 0);
    
    if(len > 0)
        return Array(len).join(padStr || '') + str;
    else 
        return str;
}
    

module.exports = leftpad;